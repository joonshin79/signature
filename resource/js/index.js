$(function() {
	$this = $('[data-wrap]');
	wrap = $this.data('wrap');
	var loding = "<div class=\"loading-container\">";
    loding+= "<div class=\"loading\"></div>"
    loding+= "<div id=\"loading-text\">loading</div>"
	loding+= "</div>";

	// 로딩호출
	//$this.append(loding);

	var nav = $('nav');
	var header = $('header');
	nav.find('>button').addClass('active');
	header.find('>.btn-menu').click(function(e){
		$(this).addClass('active');
		nav.find('>button').removeClass('active');
		nav.animate({left: "0"},300);
		nav.find('>aside>ol>li>strong').click(function(e){
			if($(this).next('ul').is(':visible')){
				$(this).addClass('active').next('ul').slideUp(100);
				return;
			} else{
				$(this).removeClass('active').next('ul').slideDown(100);
				return;
			}
		});
	});
	nav.find('>button').click(function(e){
		$(this).addClass('active');
		header.find('>.btn-menu').removeClass('active');
		$('nav').animate({left: "-110vw"},300);
	});
	header.find('>.btn-search').click(function(e){
		$('.layer-search').show();
	});
	$('.layer-search>.btn-close').click(function(e){
		$('.layer-search').hide();
	});

    $('.brand-container>article>.thomb>ul').each(function (i, item) {
        if($(this).text().length > 170){
        	$(this).addClass('fs14');
        }
    });
	$.urlParam = function(name){
		var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
		if (results==null){
		   return null;
		}
		else{
		   return results[1] || 0;
		}
	}

	var product_seq = 'product.jsp?product_seq='+$.urlParam('product_seq');
    $('nav ol').find('a').each(function (i, item) {
        var href = $(this).attr('href');
		if(product_seq == href){
			$(this).addClass('active');
		}
    });
});
